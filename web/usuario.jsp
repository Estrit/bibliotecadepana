<%-- 
    Document   : usuario
    Created on : 13-07-2021, 2:19:14
    Author     : Striit
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="cl.entities.Usuario"%>
<%@page import="java.util.List"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="cl.model.ServicioLocal"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%! ServicioLocal servicio; %>
<%    
    InitialContext ctx = new InitialContext(); 
    servicio=(ServicioLocal) ctx.lookup("java:global/GestionVenta2021/Servicio!cl.model.ServicioLocal");
    List<Usuario> lista = servicio.getUsuarios();
%>
<c:set scope="page" var="lista" value="<%=lista%>"/>

<!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>

        <div class="row">
            <div class="col s6">
                <h3>CATEGORIA</h3>
                <form action="control.do" method="post">
                    <div class="input-field col s6">
                        <input id="nombre" type="text" id="nombre">
                        <label for="nombre">Nombre</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="desc" type="text" id="desc">
                        <label for="desc">Descripcion</label>
                    </div>
                    <button class="btn right" name="bt" value="addcat" type="submit">
                        Guardar
                    </button>
                </form>
                <br><br>
                <table class="bordered">
                    <tr>
                        <td>CÓDIGO</td>
                        <td>NOMBRE</td>
                        <td>ESTADO</td>
                        <td></td>
                    </tr>
                    <c:forEach items="${lista}" var="c">
                        <tr>
                            <td>${c.codigocategoria}</td>
                            <td>$</td>
                            <td></td>
                        </tr>
                    </c:forEach>


                </table>
            </div>
        </div>
        
        
        
      <!--JavaScript at end of body for optimized loading-->
      <script type="text/javascript" src="js/materialize.min.js"></script>
    </body>
  </html>
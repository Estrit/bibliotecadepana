/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.model;

import javax.ejb.Stateless;
import java.util.*;
import cl.entities.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 *
 * @author Striit
 */
@Stateless
public class Servicio implements ServicioLocal {

    @PersistenceContext(unitName = "GestionVenta2021PU")
    private EntityManager em;

    @Override
    public void insertar(Object o) {
        em.persist(o);
    }

    @Override
    public void sincronizar(Object o) {
        em.merge(o);
        em.flush();
    }

    @Override
    public Categoria buscarCategoria(int codigo) {
        return em.find(Categoria.class, codigo);
    }

    @Override
    public void editarCategoria(int codigo, int estado) {
        //Primero se debe buscar la categoria
        Categoria cat=buscarCategoria(codigo);
        cat.setEstado(estado);
        em.merge(cat);
        em.flush();
        em.refresh(cat);
    }

    @Override
    public List<Categoria> getCategorias() {
        return em.createQuery("select c from Categoria c").getResultList();
    }

    @Override
    public Usuario buscarUsuario(String rut) {
        return em.find(Usuario.class, rut);
    }

    @Override
    public void editarUsuario(String rut, String clave) {
        //Primero Buscar al Usuario
        Usuario user=buscarUsuario(rut);
        user.setClave(clave);
        em.merge(user);
        em.flush();
        em.refresh(user);
    }

    @Override
    public List<Usuario> getUsuarios() {
        return em.createQuery("select u from Usuario u").getResultList();
    }

    @Override
    public Producto buscarProducto(int codigo) {
        return em.find(Producto.class, codigo);
    }

    @Override
    public void editarProducto(int codigo, int precio, int stock, int estado) {
        //Primero buscar el producto
        Producto p=buscarProducto(codigo);
        p.setPrecio(precio);
        p.setStock(p.getStock()+stock);
        p.setEstado(estado);
        em.merge(p);
        em.flush();
        em.refresh(p);
    }

    @Override
    public List<Producto> getProductos() {
        return em.createQuery("select p from Producto p").getResultList();
    }

    @Override
    public Usuario iniciarSesion(String rut, String clave) {
        //Primero es buscar si existe el usuario
        Usuario user=buscarUsuario(rut);
        return (user!= null && user.getClave().equals(clave))?user:null;
    }

    

    

    
}
